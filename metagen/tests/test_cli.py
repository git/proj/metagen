import os
import sys
import unittest
from contextlib import redirect_stdout
from io import StringIO
from unittest.mock import patch

from metagen.__main__ import main


class TestCli(unittest.TestCase):

    def test_missing_type(self):
        self.assertExitFail("-m", "-Q")
        self.assertExitFail("-e", "mail@example.org", "-Q")

    def test_echangelog_user(self):
        test_env = {"ECHANGELOG_USER": "First Last <mail@example.org>"}
        test_args = ["-m", "-Q", "-t", "person"]

        with patch.dict(os.environ, test_env):
            self.assertExitSuccess(*test_args)

        with patch.dict(os.environ, clear=True):
            self.assertExitFail(*test_args)

    def test_maint_desc(self):
        self.assertExitSuccess("-e", "someguy@gentoo.org",
                               "-d", "Maint desc", "-Q", "-t", "person")
        self.assertExitSuccess("-e", "someguy@gentoo.org",
                               "-d", "Maint desc", "-n", "Jon Doe",
                               "-Q", "-t", "person")

    def test_echangelog_user_long(self):
        test_env = {"ECHANGELOG_USER": "First Last <mail@example.org>"}
        test_args = ["-m", "-e", "foo@bar.com",
                     "-d", "Foo bar.,Chow fun", "-Q", "-t", "person"]

        with patch.dict(os.environ, test_env):
            self.assertExitSuccess(*test_args)

        with patch.dict(os.environ, clear=True):
            self.assertExitFail(*test_args)

    def test_missing_email(self):
        self.assertExitFail("-Q", "-t", "person")
        self.assertExitFail("-l", "Long desc", "-Q", "-t", "person")
        self.assertExitFail("-d", "Maintainer desc", "-Q", "-t", "person")

    def assertExitFail(self, *args):
        with self.assertRaises(SystemExit) as cm:
            self._run_metagen(*args)
        self.assertEqual(cm.exception.code, 1)

    def assertExitSuccess(self, *args):
        self._run_metagen(*args)

    def _run_metagen(self, *args):
        with redirect_stdout(StringIO()):
            with patch.object(sys, "argv", ["metagen", *args]):
                main()
